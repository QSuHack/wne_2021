

#include "seria_do_egzaminu.h"

class Pomiary {
public:
    std::vector<int> _pomiary;

    Pomiary() {};

    void add(int value) { _pomiary.push_back(value); }

    int ile_pomiarow() {
        return _pomiary.size();
    }

    double srednia() {
        double suma = 0;
        for (auto el: _pomiary) {
            suma += el;
        }
        return suma / _pomiary.size();
    }

    int najwiekszy() {
        if (_pomiary.size() > 0) {
            std::sort(_pomiary.begin(), _pomiary.end());
            return *(_pomiary.end() - 1);
        }
        return -1; //gdy nie ma pomiarów zwróć -1 jako błąd
    }
};

class Napis {
public:
    std::string value = "";

    Napis() {};

    Napis(char *t) : value(t) {};

    void dolacz(char znak) {
        std::string v(1, znak);
        value.append(v);
    }

    char znak(int pozycja) {
        return value.at(pozycja);
    }

    void wypisz() {
        std::cout << value;
    }
};

struct Punkt {
    double x;
    double y;
};

//class Trojkat{
//public:
//    Punkt _p1,_p2,_p3;
//    Trojkat(Punkt p1, Punkt p2, Punkt p3) : _p1(p1), _p2(p2),_p3(p3){};
//    void przesun(double dx, double dy){
//        _p1.x += dx;_p2.x += dx;_p3.x += dx;
//        _p1.y += dy;_p2.y += dy;_p3.y += dy;
//    }
//    void nalezy(Punkt p){
//        //TODO
//    }
//    Trojkat operator== (Trojkat b){
//        if ()
//    }
//};
class Osoba {
public:
    std::string _name, _surname;
    int _wiek;

    Osoba(char *imie, char *nazwisko) : _name(imie), _surname(nazwisko) {};

    void ustaw_wiek(int wiek) { _wiek = wiek; }

    bool starszaNiz(const Osoba &osoba) {
        return (this->_wiek > osoba._wiek);
    }

    bool imiennik(const Osoba &osoba) {
        return (this->_name == osoba._name);
    }
};

class TablicaPosortowana {
public:
    int max_size_;
    std::vector<int> vec;

    TablicaPosortowana(int max_size) : max_size_(max_size) {};

    void dodaj(int value) {
        if (vec.size() < max_size_) {
            vec.push_back(value);
            std::sort(vec.begin(), vec.end());
        }
    }

    bool usun(int value) {
        for (auto it = vec.begin(); it < vec.end(); it++) {
            if (*it == value) {
                it = vec.erase(it);
                return true;
            }
        }
        return false;
    }

    int element(int indeks) {
        return vec.at(indeks);
    }

    int max() {
        return vec.back();
    }
};

class Konto {
private:
    int konto = 0;
    int debet = 0;
    int sumauznan = 0;
    int sumaobciazen = 0;
public:
    Konto() {};

    int stan_konta() {
        return this->konto;
    }

    void zmien(int oile) {
        if ((konto + oile + debet) >= 0) {
            konto += oile;
            sumaobciazen += oile > 0 ? 0 : oile;
            sumauznan += oile > 0 ? oile : 0;
        }
    }

    void ustanowdebet(int num) {
        debet = num;
    }

    int sumaUznan() {
        return sumauznan;
    }

    int sumaObciazen() {
        return sumaobciazen;
    }
};

class Karta {
public:
    int kolor, ranga;
};

class ZbiorKart {
    std::vector<Karta> vec;
public:
    ZbiorKart() {};

    void dodaj(Karta k) {
        vec.push_back(k);
    }

    Karta losowa() {
        int a = std::rand() % vec.size();
        return vec.at(a);
    }

// sekwens: TODO
};

class Trojmian {
    double a, b, c;
public:

    Trojmian(double a_, double b_, double c_) : a(a_), b(b_), c(c_) {};

    Trojmian(double x1, double x2) : a(1), b(-(x1 + x2)), c(x1 * x2) {};

    int ile_pierwiastkow() {
        if (b * b - 4 * a * c > 0) { return 2; }
        return ((b * b - 4 * a * c) == 0) ? 1 : 0;
    }

    void pomnoz(double s) {
        a *= s;
        b *= s;
        c *= s;
    }

    void drukuj() {
        std::cout << "a:" << a << "b: " << b << "c: " << c;
    }

};

class Tablica {
    std::map<int, int> tab;
public:
    Tablica() {};

    void ustaw(int indeks, int wartosc) {
        tab.insert_or_assign(indeks, wartosc);
    }

    int wartosc(int indeks) {
        try {
            return tab.at(indeks);
        }
        catch (std::out_of_range) {
            return 0;
        }
    }

    int suma() {
        int s = 0;
        for (auto x: tab) {
            s += x.second;
        }
        return s;
    }

    void ustaw(int a, int b, int value) {
        for (int i = a; i < b; i++) {
            ustaw(i, value);
        }
    }

    void przesun() {
        std::map<int, int> n;
        for (auto x: tab) {
            n.insert_or_assign(x.first + 1, x.second);
        }
        tab.clear();
        tab = n;
    }
};

class Mapa {
    // E26
    std::map<int, std::vector<double>> internal_map;
public:
    Mapa() {};

    void dodaj(int miasto, double x, double y, int mieszk) {
        std::vector<double> tmp = {x, y, (double) mieszk};
        internal_map.try_emplace(miasto, tmp);
    }

    void ustaw_mieszk(int miasto, int mieszk) {
        internal_map.at(miasto).at(2) = (double) mieszk;
    }

    int najblizsze(double x, double y) {
        int d = INT32_MAX;
        int c = 0;
        int t = 0;
        for (auto city: internal_map) {
            t = odleglosc(city.second.at(0), city.second.at(1), x, y);
            if (t < d) {
                d = t;
                c = city.first;
            }
        }
        return c;
    }

    int mieszkancy(double x, double y, double r) {
        int m = 0;
        for (auto city: internal_map) {
            if (odleglosc(city.second.at(0), city.second.at(1), x, y) < r) {
                m += city.second.at(2);
            }
        }
        return m;
    }

    void usun_miasto(int miasto) {
        internal_map.erase(miasto);
    }

    double odleglosc(double x1, double y1, double x2, double y2) {
        return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }
};

class Urna {
//it could be done better xD
//E29
    std::vector<int> u;
    std::vector<std::pair<int, int>> r;
    int w = 0;
    int w_done = 0;
public:
    Urna(int ile_kandydatow, int ile_wyborcow) {
        u.assign(ile_kandydatow, 0);
        w = ile_wyborcow;
    }

    void glos(int kandydat) {
        if (w_done < w) {
            w_done += 1;
            u.at(kandydat) += 1;
        }
    }

    int ile_na_kandydata(int kandydat) {
        return u.at(kandydat);
    }

    double frekwencja() {
        return w_done / w;
    }

    bool druga_tura() {
        help();
        return (r.at(0).second * 2 > w_done);
    }

    void raport() {
        help();
        for (auto record: r) {
            std::cout << record.first << "  " << record.second / w_done * 100 << "%";
        }
    }

    void help() {
        r.clear();
        for (int i = 0; i < u.size(); i++) {
            r.push_back(std::pair(i, u.at(i)));
        }
        std::sort(r.begin(), r.end(), std::greater<>()); //sorting in descending order c++14
    }
};

class Lamana {
    std::vector<std::pair<double, double>> internal;
    std::vector<double> length;

public:
    Lamana(double x, double y) {
        internal.push_back(std::pair(x, y));
    }

    void dodaj_na_koncu(double x, double y) {
        length.push_back(odleglosc(internal.back().first, internal.back().second, x, y));
        internal.push_back(std::pair(x, y));
    }

    double odleglosc(double x1, double y1, double x2, double y2) {
        //copied from Miasto class
        return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

    double dlugosc() {
        double s = 0;
        for (auto i: length) {
            s += i;
        }
        return s;
    }

    double najdluszy() {
        double n = -1;
        for (auto i: length) {
            if (i > n) {
                n = i;
            }
        }
        return n;
    }

    bool powtorzenie() {
        for (auto i = internal.begin(); i < internal.end(); i++) {
            for (auto j = internal.begin(); j < internal.end(); j++) {
                if (i != j && (*i == *j)) {
                    return true;
                }
            }
        }
        return false;
    }
};

class Odcinek_32 {
    double a, b;
    bool pusty = false;
    bool punkt = false;

    Odcinek_32(double a_, double b_) {
        a = a_;
        b = b_;
        if (a==b){
            punkt=true;
        }
        if(a>b){
            pusty=true;
        }
    }
    bool zawiera(double x){
        if (x>a && x< b){
            return true;
        }
        return false;
    }
//    TODO to implement
//    Odcinek_32 czescWspolna(Odcinek_32& odc){
//        if (odc.pusty || this->pusty){
//            return Odcinek_32(0,-1);
//        }
//        return Odcinek_32()
//    }
};

void test_E1() {
    Pomiary a = Pomiary();
    a.add(10);
    a.add(20);
    a.add(30);
    std::cout << "A1 " << (a.ile_pomiarow() == 3) << (a.najwiekszy() == 30) << (a.srednia() == 20);
}

void test_E2() {
    Napis a = Napis();
    std::cout << "A2: " << (a.value == "");
    a.dolacz('a');
    a.dolacz('b');
    std::string tmp = "9x001y2";
    char *x = (char *) tmp.c_str();
    Napis c(x);
    c.dolacz('v');
    std::cout << " ";
    a.wypisz();
    std::cout << " ";
    c.wypisz();
}

void test_E4() {
    std::string tmp = "Jan";
    char *x = (char *) tmp.c_str();
    std::string tmp2 = "Kowalski";
    char *y = (char *) tmp2.c_str();
    std::string tmp3 = "Andrzej";
    char *z = (char *) tmp3.c_str();
    std::string tmp4 = "Malinowski";
    char *zz = (char *) tmp4.c_str();
    Osoba a = Osoba(x, y);
    a.ustaw_wiek(10);
    Osoba b = Osoba(x, zz);
    b.ustaw_wiek(20);
    Osoba c = Osoba(z, zz);
    c.ustaw_wiek(9);


    std::cout << "E4: " << a.starszaNiz(c) << a.starszaNiz(b) << a.imiennik(b) << a.imiennik(c); //1010
}

void test_E5() {
    TablicaPosortowana a = TablicaPosortowana(5);
    a.dodaj(10);
    a.dodaj(5);
    a.dodaj(3);
    a.dodaj(102);
    a.dodaj(12);
    a.dodaj(11);
    std::cout << a.usun(11);
    std::cout << a.usun(3);
    std::cout << a.element(0) << a.element(1) << a.element(2) << "  " << a.max();
}

void test_E6() {
    Konto a, b;
    a.zmien(100);
    b.zmien(10);
    a.zmien(-12);
    a.zmien(-120);
    b.zmien(1);
    a.zmien(1000);
    b.ustanowdebet(10);
    b.zmien(-20);
    b.zmien(-10);
    std::cout << "E6: " << (a.stan_konta() == (100 - 12 + 1000))
              << (a.sumaObciazen() == -12) << (a.sumaUznan() == 1100) <<
              (b.stan_konta() == -9);
}

// Czas i Set (E8 & E9) w classes_8.cpp
void test_E17() {
    Trojmian a = Trojmian(2, 2);
    a.drukuj();
    Trojmian b = Trojmian(4, 5);
    std::cout << a.ile_pierwiastkow() << " " << b.ile_pierwiastkow();
    b.drukuj();
    a.pomnoz(2.4);
    a.drukuj();
}

void test_E18() {
    Tablica t = Tablica();
    t.ustaw(0, 10);
    t.ustaw(10, 11);
    t.wartosc(2);
    t.przesun();
    t.wartosc(1);
}

void test_exam_series() {
    test_E18();
    //test_E5();
    //test_E17();
//    test_E4();
//    test_E6();
}