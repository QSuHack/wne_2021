
#include "classes_9.h"

class Rational{
public:
    int a,b;
    Rational(int a = 0, int b = 1){
        this->a = a; this->b = b;
        int tmp = nwd(this->a, this->b);
        this->b = this->b/tmp;
        this->a  = this->a/tmp;
        if (b==0){
            throw std::invalid_argument("Denominator cannot be 0");
        }
    }
    Rational operator+ ( Rational r){
        this->a  = (this->a*r.b+r.a*this->b );
        this->b = this->b* r.b;
        int tmp = nwd(this->a, this->b);
        this->b = this->b/tmp;
        this->a  = this->a/tmp;
        return *this;
    }
    Rational operator- (){
        this->a = -(this->a);
        return *this;
    }
    Rational operator- (Rational r){
        return *this + (-r);
    }
    Rational operator* ( Rational r){
        this->a  =this->a*r.a;
        this->b = this->b* r.b;
        int tmp = nwd(this->a, this->b);
        this->b = this->b/tmp;
        this->a  = this->a/tmp;
        return *this;
    }
    Rational operator/ (Rational r){
        std::swap(r.a ,r.b);
        return *this * r;
    }

private:
    int nwd(int a, int b){
        if (b==0){
            return a;
        }
        return nwd(b, a%b);
    }
};
std::ostream&  operator<<(std::ostream& os, Rational r){
    std::cout << r.a <<"/"<< r.b;
    return os;
}
void test_9(){
   Rational a = Rational(1, 9);
   Rational b = Rational();
   a =a+ Rational(2);
   std::cout <<a << " ";
   a = a - Rational(1,3);
   std::cout<<a;
   std::cout << " "<< b;
    std::cout << " "<<   Rational(16)/Rational(4);
}