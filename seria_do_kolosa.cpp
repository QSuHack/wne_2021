

#include "seria_do_kolosa.h"

//najpierw definicje funkcji wymaganych przez polecenia, funkcje pomocnicze, funkcje testujace poszczegolne, test całości
//w kolejności użycia wg powyższych kategorii
int tylkoDodatnie(int *t, int rozmiar, int *d) {
    int j = 0;
    for (int i = 0; i < rozmiar; i++) {
        if (*(t + i) > 0) {
            *(d + j) = *(t + i);
            j++;
        }
    }
    return j;
}

bool znajdzRowne(int *t, int rozmiar) {
    for (int i = 0; i < rozmiar; i++) {
        for (int j = 0; j < rozmiar; j++) {
            if (i != j) {
                if (*(t + i) == *(t + j)) {
                    return true;
                }
            }
        }
    }
    return false;
}

void wspak(int **t, int rozmiar) {
    int *p = (*t + rozmiar);
    int *k;
    k = new int[rozmiar];
    for (int i = 0; i < rozmiar; i++) {
        *(k + i) = *(p - i - 1);
    }
    *t = k;
}

void wspak(int *t, int rozmiar) {
    for (int i = 0; i < rozmiar / 2; i++) {
        std::swap(*(t + i), *(t + rozmiar - i - 1));
    }
}

int sumaDzielnikow(int n) {
    int suma = 0;
    if (n < 1) {
        return -1;
    }
    int k = 1;
    while (k < n / 2 + 1) {
        if (n % k == 0) {
            suma += k;
            if (n / k != k) {
                suma += n / k;
            }
        }
        k++;
    }
    return suma;
}

void usunSpacje(const char *s, char *t) {
    int j = 0;
    for (int i = 0; *(s + i); i++) {
        if (*(s + i) != ' ') {
            *(t + j) = *(s + i);
            j++;
        }
    }
}

int pierwiastek(int n) {
    //metoda babilońska
    double x = n / 2;
    for (int i = 0; i < 50; i++) {
        x = (x + n / x) / 2;
    }
    return floor(x);

}

int sumaRoznych(int *t, int rozmiar) {
    std::set<int> tmp;
    int suma = 0;
    for (int i = 0; i < rozmiar; i++) {
        tmp.insert(*(t + i));
    }
    for (auto it: tmp) {
        suma += it;
    }
    return suma;
}

int fibo(int n) {
    if (n == 0 || n == 1) {
        return 1;
    }
    return fibo(n - 1) + fibo(n - 2);
}

int drugi(int *t, int rozmiar) {
    int a = *t > *(t + 1) ? *t : *(t + 1);
    int b = *t > *(t + 1) ? *(t + 1) : *t;
    if (rozmiar > 2) {
        for (int i = 2; i < rozmiar; i++) {
            if (*(t + i) > a) {
                b = a;
                a = *(t + i);
            } else {
                if (*(t + i) > b) {
                    b = *(t + i);
                }
            }
        }
    }
    return b;
}

void przesun(int *t, int rozmiar) {
    int tmp = *(t + rozmiar - 1);
    int tmp2 = *t;
    for (int i = 0; i < rozmiar; i++) {
        std::swap(tmp2, *(t + i));
    }
    *(t) = tmp;
}

int suma_pierwszych(int n) {
    int suma = 0;
    if (n < 2) {
        return 0;
    }
    std::vector<bool> t(n + 1, true);
    t.at(0) = t.at(1) = false;
    for (int i = 2; i <= n; i++) {
        if (t.at(i)) {
            for (int j = i + 1; j <= n; j++) {
                if (j % i == 0) {
                    t.at(j) = false;
                }
            }
        }
    }
    for (int i = 2; i < t.size(); i++) {
        if (t.at(i)) {
            suma += i;
        }
    }
    return suma;
}

bool f_rozne_znaki(char *s) {
    for (int i = 0; *(s + i + 1); i++) {
        if (*(s + i) == *(s + i + 1)) {
            return false;
        }
    }
    return true;
}

double mediana(double *t, int rozmiar) {
    std::vector<double> tmp;
    tmp.reserve(rozmiar);
    for (int i = 0; i < rozmiar; i++) {
        tmp.push_back(*(t + i));
    }
    std::sort(tmp.begin(), tmp.end());
    if (tmp.size() % 2 == 0) {
        return (tmp.at(tmp.size() / 2 - 1) + tmp.at(tmp.size() / 2)) / 2;
    }
    return tmp.at((tmp.size() + 1) / 2 - 1);
}

int suma_cyfr(int n) {
    int suma = 0;
    while (n > 0) {
        suma += n % 10;
        n /= 10;
    }
    return suma;
}

bool n_same_character(char *s, int n) {
    if (*(s + n - 1) == '\0') {
        return false;
    }
    bool flag;
    for (int i = 0; *(s + i + n - 1); i++) {
        flag = true;
        for (int j = i; j < i + n - 1; j++) {
            if (flag) {
                if (*(s + j) != *(s + j + 1)) {
                    flag = false;
                }
            }
        }
        if (flag) {
            return true;
        }
    }

    return false;
}

bool is_prime(int number) {
    if (number < 2) {
        return false;
    }
    if (number == 2) {
        return true;
    }
    if (number % 2 == 0) {
        return false;
    }
    for (int i = 3; i < ceil(sqrt(number)) + 1; i++) {
        if (number % i == 0) {
            return false;
        }
    }
    return true;
}

int vary_prime(int *t, int n) {
    std::set<int> myset;
    for (int i = 0; i < n; i++) {
        if (is_prime(*(t + i))) {
            myset.insert(*(t + i));
        }
    }
    return myset.size();
}

bool trojka_pitagorejska(int n) {
    for (int a = 1; a < n; a++) {
        for (int b = 1; b < n; b++) {
            if (n * n == a * a + b * b) {
                return true;
            }
        }
    }
    return false;
}

int h_vary_chars(char *s) {
    std::set<char> myset;
    for (int i = 0; *(s + i); i++) {
        myset.insert(*(s + i));
    }
    return myset.size();
}

int most_common_value(int *t, int n) {
    std::map<int, int> mp;

    for (int i = 0; i < n; i++) {
        mp[*(t + i)]++;
    }
    int max_value = 0;
    std::vector<int> max_it;
    for (auto a: mp) {
        if (a.second > max_value) {
            max_it.clear();
            max_it.push_back(a.first);
            max_value = a.second;
        }
        if (a.second == max_value) {
            max_it.push_back(a.first);
        }
    }
    std::sort(max_it.begin(), max_it.end(), std::greater<>());
    return max_it.at(0);
}

bool arytmentyczny(int *t, int r) {
    int diff = *(t) - *(t + 1);
    for (int i = 1; i < r - 1; i++) {
        if ((*(t + i) - *(t + i + 1)) != diff) {
            return false;
        }
    }
    return true;
}

void space_remover(char *s) {
    bool is_space = false;
    for (int i = 0; *(s + i); i++) {
        if (*(s + i) != ' ') {
            is_space = false;
            std::cout << *(s + i);
            continue;
        }
        std::cout << "*";
    }
}

bool two_values(int *t, int r) {
    int a = *t;
    int b;
    bool first_change = true;
    for (int i = 1; i < r; i++) {
        if (a != *(t + i) && first_change) {
            b = *(t + i);
            first_change = false;
        } else {
            if (*(t + i) != a && *(t + i) != b) {
                return false;
            }
        }
    }
    return !(first_change);
}

bool two_values_on_set(int *t, int r) {
    std::set<int> myset;
    for (int i = 1; i < r; i++) {
        myset.insert(*(t + i));
    }
    return (myset.size() == 2);
}

bool crossed_letters_on_vectors(char *s, char *t) {
    std::vector<char> a, b;
    for (int i = 0; *(s + i); i++) {
        a.push_back(*(s + i));
    }
    for (int i = 0; *(t + i); i++) {
        b.push_back(*(t + i));
    }
    auto it2 = b.begin();
    for (auto it = a.begin(); it != a.end();) {
        if (*it != *it2) {
            it = a.erase(it);
        } else {
            if ((it2 + 1) != b.end()) {
                it2++;
                it++;
                continue;
            }
            return true;
        }
    }
    return (a == b);

}

bool stricly_growing_series(int *t, int r) {

    if (r < 3) {
        return false;
    }
    int diff = *(t + 1) - *t;
    for (int i = 2; i < r; i++) {
        if (*(t + i) - *(t + i - 1) > diff) {
            diff = *(t + i) - *(t + i - 1);
        } else {
            return false;
        }
    }
    return true;
}

void three_letters_only(char *s) {
    std::vector<char> tmp;
    for (int i = 0; *(s + i); i++) {
        if (*(s + i) == ' ') {
            if (tmp.size() == 3) {
                std::cout << tmp.at(0) << tmp.at(1) << tmp.at(2) << " ";

            }
            tmp.clear();
        } else {
            tmp.push_back(*(s + i));
        }
    }
    if (tmp.size() == 3) {
        std::cout << tmp.at(0) << tmp.at(1) << tmp.at(2) << " ";
    }
}

bool not_a_divider(int *t, int r) {
    std::vector<int> numbers;
    for (int i = 0; i < r; i++) {
        if (std::count(numbers.begin(), numbers.end(), *(t + i))) {
            return false;
        }
        numbers.push_back(*(t + i));
    }
    for (int m: numbers) {
        for (int k: numbers) {
            if (m % k == 0 && m != k) {
                return false;
            }
        }
    }
    return true;
}

bool difrent_and_positive(int *t, int r) {
    std::vector<int> nums;
    for (int i = 0; i < r; i++) {
        if (*(t + i) > 0 && !std::count(nums.begin(), nums.end(), *(t + i))) {
            nums.push_back(*(t + i));
        } else {
            return false;
        }
    }
    return true;
}

void ab_is_cancelled(char *s) {
    //assumption: just one * per ABAB
    bool is_cancelled = false;
    int i;
    for (i = 0; *(s + i + 1); i++) {
        if (*(s + i) == 'A' && *(s + i + 1) == 'B') {
            if (!is_cancelled) { std::cout << "*"; }
            is_cancelled = true;
            i++;
        } else {
            is_cancelled = false;
            std::cout << *(s + i);
        }
    }
    std::cout << *(s + i);
}

int greater_than_neighbor(int *t, int n) {
    int sum = 0;
    if (*t > *(t + 1)) {
        sum += *t;
    }
    for (int i = 1; i < n - 1; i++) {
        if (*(t + i) > *(t + i - 1) && *(t + i) > *(t + i + 1)) {
            sum += *(t + i);
        }
    }
    if (*(t + n - 1) > *(t + n - 2)) {
        sum += *(t + n - 1);
    }
    return sum;
}

void once_printed(char *s) {
    char last_chr = *s;
    std::cout << last_chr;
    for (int i = 1; *(s + i); i++) {
        if (last_chr != *(s + i)) {
            last_chr = *(s + i);
            std::cout << *(s + i);
        }
    }
}

void even_index(char *s) {
    for (int i = 0; *(s + i); i += 2) {
        std::cout << *(s + i);
    }
}

int once_sum(int *t, int n) {
    std::map<int, int> mp;
    int sum = 0;
    for (int i = 0; i < n; i++) {
        mp[*(t + i)]++;
    }
    for (auto a: mp) {
        if (a.second == 1) {
            sum += a.first;
        }
    }
    return sum;
}

void first_letters(char *s) {
    bool first = true;
    for (int i = 0; *(s + i); i++) {
        if (*(s + i) == ' ') {
            first = true;
            continue;
        }
        if (first) {
            std::cout << *(s + i);
            first = false;
        }
    }
}

bool not_a_sum(int *t, int r) {
//it's kinda monster but why not
    for (int i = 0; i < r; i++) {
        for (int j = 0; j < r; j++) {
            for (int g = 0; g < r; g++) {
                if (i != g && i != j && g != j) {
                    if (*(t + i) == (*(t + g) + *(t + j))) {
                        return false;
                    }
                }
            }
        }
    }
    return true;
}

void print_index_letter(char *s) {
    for (int i = 0; *(s + i); i++) {
        for (int j = 1; j <= (i + 1); j++) {
            std::cout << *(s + i);
        }
    }
}

int gcd(int a, int b) {
    if (b == 0) {
        return a;
    }
    return gcd(b, a % b);
}

bool is_coprime(int *t, int r) {
    std::set<int> myset;
    for (int i = 0; i < r; i++) {
        myset.insert(*(t + i));
    }
    for (auto i: myset) {
        for (auto j: myset) {
            if (i != j) {
                if (gcd(i, j) != 1) {
                    return false;
                }
            }
        }
    }
    return true;
}

void stars_are_cancelled(char *s) {
    bool begining = true;
    int j;
    for (j = 0; *(s + j); j++); //finding end
    for (j = j - 1; *(s + j) == '*'; j--); //finding last position of non-star element
    for (int i = 0; i <= j; i++) {
        if (*(s + i) != '*') {
            begining = false;
        }
        if (begining && *(s + i) == '*') {
            continue;
        }
        std::cout << *(s + i);
    }
}

bool h_check(int *t, int n, int k) {
    int tmp;
    for (int i = 0; i < n; i++) {
        tmp = *(t + i);
        for (int j = i + k; j + i <= n; j += k) {
            if (*(t + j) != tmp) {
                return false;
            }
        }
    }
    return true;
}

int h(int *t, int n) {
    for (int i = 1; i < n; i++) {
        if (h_check(t, n, i)) {
            return i;
        }
    }
    return n;
}

void stars_printing(int n) {
    std::string a, b;
    for (int i = 0; i < n; i++) {
        std::cout << std::string(n - i, '*');
        std::cout << std::string(i, '.') << std::endl;
    }
}

bool diff_table(int *t, int n, int d) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i != j && abs(*(t + i) - *(t + j)) < d) {
                return false;
            }
        }
    }
    return true;
}

bool is_digit(char c) {
    return (c >= '0' && c <= '9');
}

void always_right(char *s) {
    int j;
    for (j = 0; *(s + j); j++);
    for (j = j - 1; j >= 0; j--) {
        if (is_digit(*(s + j))) {
            break;
        }
    }
    char num = *(s + j);
    for (int i = 0; *(s + i); i++) {
        if (is_digit(*(s + i))) {
            std::cout << num;
            num = *(s + i);
        } else {
            std::cout << *(s + i);
        }
    }
}

bool rising_table(int *t, int n) {
    int sum = 0;
    for (int i = 0; i < n; i++) {
        if (*(t + i) > sum) {
            sum += *(t + i);
        } else {
            return false;
        }
    }
    return true;
}

void losuj_tablice(int *tab, int size, int min_range, int max_range) {
    for (int i = 0; i < size; i++) {
        *(tab + i) = rand() % max_range + min_range;
    }
}

template<typename T>
void show_array(T *arr, int size) {
    for (int i = 0; i < size; i++) {
        std::cout << *(arr + i) << " ";
    }
}

void show_array(char *arr) {
    for (int i = 0; *(arr + i); i++) {
        std::cout << *(arr + i);
    }
}

void test_A1(int n) {
    int *tab1, *tab2;
    tab1 = new int[n];
    tab2 = new int[n];
    losuj_tablice(tab1, n, -10, 100);
    show_array(tab1, n);
    std::cout << "\n";
    show_array(tab2, tylkoDodatnie(tab1, n, tab2));

}

void test_A2() {
    int tab[7] = {1, 2, 3, 4, 5, 2, 5};
    int tab2[7] = {1, 2, 3, 4, 5, 7, 8};
    std::cout << "\nA2 :" << znajdzRowne(tab, 7) << " " << znajdzRowne(tab2, 7);
}

void test_A3() {
    int *tab;
    tab = new int[7];
    losuj_tablice(tab, 7, 0, 10);
    std::cout << "\nA3(Base):";
    show_array(tab, 7);
    wspak(&tab, 7);
    std::cout << "\nA3: ";
    show_array(tab, 7);
    std::cout << "\nA3(second):";
    wspak(tab, 7);
    show_array(tab, 7);

}

void test_A4() {
    std::cout << "A4: " << sumaDzielnikow(5);
}

void test_A5() {
    std::string tmp = "nciofwtp ew  frio rgp f";
    char *a = (char *) tmp.c_str();
    char *n = new char[tmp.length() + 1];
    usunSpacje(a, n);
    std::cout << "\nA5: ";
    show_array(a);
    std::cout << "\n";
    show_array(n);
}

void test_A6(int min_r, int max_r) {
    for (int i = min_r; i < max_r + 1; i++) {
        std::cout << i << "-->" << pierwiastek(i) << " ?= " << sqrt(i) << "\n";
    }
}

void test_A7() {
    int tab[7] = {1, 2, 3, 4, 5, 2, 5};
    std::cout << "A7: " << sumaRoznych(tab, 7);
}

void test_A8() {
    int n = 10;
    std::cout << "A8: " << n << fibo(n);
}

void test_A9() {
    int tab[7] = {1, 7, 3, 4, 5, 2, 6};
    std::cout << "A9: " << drugi(tab, 7);
}

void test_A10() {
    int tab[7] = {1, 7, 3, 4, 5, 2, 6};
    std::cout << "A10: ";
    show_array(tab, 7);
    przesun(tab, 7);
    std::cout << "\n";
    show_array(tab, 7);
}

void test_A11(int n) {
    std::cout << "A11: " << suma_pierwszych(n);
}

void test_A12() {
    std::string tmp = "mpfinrwca";
    char *a = (char *) tmp.c_str();
    std::cout << "A12: " << f_rozne_znaki(a);
}

void test_A13(int size) {
    double *tab;
    tab = new double[size];
    for (int i = 0; i < size; i++) {
        *(tab + i) = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
    }
    std::cout << "A13 :";
    show_array(tab, size);
    std::cout << "M:" << mediana(tab, size);
}

void test_A14(int number) {
    std::cout << "A14 :" << suma_cyfr(number);
}

void test_A15() {
    std::string tmp = "ssmpfinaaa";
    char *a = (char *) tmp.c_str();
    std::cout << "A15: " << n_same_character(a, 2);
}

void test_A16() {
    int tab[9] = {11, 7, 3, 4, 50, 17, 11, 15, 6};
    std::cout << "A16: " << vary_prime(tab, 9);
}

void test_A17() {
    std::cout << "A17 " << trojka_pitagorejska(13) << trojka_pitagorejska(8) << trojka_pitagorejska(20);
}

void test_A18() {
    std::string tmp = "mpfinnrwcafffmm";
    char *a = (char *) tmp.c_str();
    std::cout << "A18 :" << h_vary_chars(a);
}

void test_A19() {
    int tab[10] = {2, 11, 4, 8, 2, 0, 29, 2, 4, 4};
    std::cout << "A19: " << most_common_value(tab, 10);
}

void test_A20() {
    int tab[6] = {2, 4, 6, 8, 10, 17};
    int tab2[6] = {2, 4, 6, 8, 10, 12};
    std::cout << "A20: " << arytmentyczny(tab, 6) << arytmentyczny(tab2, 6);
}

void test_A21() {
    std::string tmp = "AB A CC CV ";
    char *a = (char *) tmp.c_str();
    std::cout << "A21:";
    space_remover(a);
}

void test_A22() {
    int tab[6] = {2, 2, 2, 3, 3, 2};
    int tab2[6] = {2, 2, 2, 2, 2, 2};
    std::cout << "A22: " << two_values(tab, 6) << two_values(tab2, 6) << two_values_on_set(tab, 6)
              << two_values_on_set(tab2, 6);
}

void test_A23() {
    std::string tmp = "ABC";
    char *a = (char *) tmp.c_str();
    std::string tmp_ = "CADCBC";
    char *b = (char *) tmp_.c_str();
    std::cout << crossed_letters_on_vectors(b, a);
    std::string tmp__ = "DADCBD";
    b = (char *) tmp__.c_str();
    std::cout << crossed_letters_on_vectors(b, a);
}

void test_A24() {
    int tab[4] = {1, 3, 6, 10};
    int tab2[6] = {1, 2, 3, 10, 2, 2};
    std::cout << "A24: " << stricly_growing_series(tab, 4) << stricly_growing_series(tab2, 6);
}

void test_A25() {
    std::string tmp = "AAA B AAAB AAA CAC";
    char *a = (char *) tmp.c_str();
    std::cout << "A25: ";
    three_letters_only(a);
}

void test_A26() {
    int tab[4] = {2, 3, 7, 11};
    int tab2[4] = {2, 3, 7, 10};
    std::cout << "A26: " << not_a_divider(tab, 4) << not_a_divider(tab2, 4);
}

void test_A28() {
    int tab[4] = {2, 3, 7, 7};
    int tab2[4] = {2, 3, 7, 10};
    std::cout << "A28: " << difrent_and_positive(tab, 4) << difrent_and_positive(tab2, 4);
}

void test_A29() {
    std::string tmp = "ABABCDEFABBA";
    char *a = (char *) tmp.c_str();
    std::cout << "A29 :";
    ab_is_cancelled(a);
}

void test_A30() {
    int tab[3] = {2, 1, 2};
    int tab2[6] = {1, 2, 3, 2, 5, 1};
    std::cout << "A30: " << greater_than_neighbor(tab, 3) << greater_than_neighbor(tab2, 6);
}

void test_A31() {
    std::string tmp = "AAABBCBAAB";
    char *a = (char *) tmp.c_str();
    std::string tmp_ = "A AAA AAA";
    char *b = (char *) tmp_.c_str();
    std::cout << "A31: ";
    once_printed(a);
    std::cout << "\n";
    once_printed(b);
}

void test_A32() {
    std::string tmp = "ABCDEF";
    char *a = (char *) tmp.c_str();
    std::cout << "A32 :";
    even_index(a);
}

void test_A33() {
    int tab[5] = {1, 1, 2, 3, 1};
    int tab2[3] = {4, 4, 4};
    std::cout << "A33: " << (once_sum(tab, 5) == 5) << (once_sum(tab2, 3) == 0);
}

void test_A34() {
    std::string tmp = " zielony   las";
    char *a = (char *) tmp.c_str();
    std::cout << "A36:";
    first_letters(a);
}

void test_A35() {
    int tab[4] = {1, 9, 3, 7};
    int tab2[5] = {3, 6, 2, 11, 4};
    std::cout << "A35: " << not_a_sum(tab, 4) << not_a_sum(tab2, 5);
}

void test_A36() {
    std::string tmp = "ABCD";
    char *a = (char *) tmp.c_str();
    std::cout << "A34:";
    print_index_letter(a);
}

void test_A37() {
    int tab[4] = {5, 13, 24, 111};
    int tab2[3] = {77, 256, 15};
    std::cout << "A37: " << is_coprime(tab, 4) << is_coprime(tab2, 3);
}

void test_A38() {
    std::string tmp = "*****A*B**C*D***";
    char *a = (char *) tmp.c_str();
    std::cout << "A38:";
    stars_are_cancelled(a);
}

void test_A39() {
    int tab[7] = {1, 2, 1, 2, 1, 2, 1};
    int tab2[6] = {1, 5, 3, 7, 1, 5};
    int tab3[7] = {2, 7, 2, 3, 7, 2, 4};
    std::cout << "A39:" << (h(tab, 7) == 2) << (h(tab2, 6) == 4) << (h(tab3, 7) == 7);
}

void test_A40() {
    std::cout << "A40: \n";
    stars_printing(5);
}

void test_A41() {
    int tab[4] = {5, 13, 24, 111};
    int tab2[3] = {77, 1256, 15};
    std::cout << "A37: " << diff_table(tab, 4, 2) << diff_table(tab2, 3, 100);
}

void test_A42() {
    std::string tmp = "9x001y2";
    char *a = (char *) tmp.c_str();
    std::cout << "A42:";
    always_right(a);
}

void test_A43() {
    int tab[4] = {1, 3, 8, 20};
    int tab2[4] = {3, 7, 11, 20};
    std::cout << "A43: " << rising_table(tab, 4) << rising_table(tab2, 4);
}

void test_s_kol() {
    srand(static_cast <unsigned> (time(0)));
//    test_A1(10);
//    test_A2();
//    test_A3();
    //  test_A4();
    //   test_A5();
    // test_A6(6,25);
//    test_A10();
//    test_A13(5);
//    test_A14(12032);
// test_A18();
//    test_A34();
//    test_A38();
//    test_A41();
    //   test_A43();
    //  test_A39();
    //  test_A33();
    test_A19();
}
// TODO:  A27