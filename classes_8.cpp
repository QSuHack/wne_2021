

#include "classes_8.h"

class Point {
    double x, y;

    friend std::ostream &operator<<(std::ostream &os, Point p);

    Point() {
        x = y = 0;
    }

    Point(double a, double b) {
        x = a;
        y = b;
    }
public:
   double distance(Point p) const {
        return sqrt((x-p.x)*(x-p.x)+(y-p.y)*(y-p.y));
    }
    void input(){
        std::cout << "Type point coordinates x and y:";
        std::cin >>x >> y;
    }
};
class Time{
    int hours, minutes, seconds;
    Time(int hours, int minutes, int seconds){
        this->hours = (hours >=0 && hours <=23)? hours : 0 ;
        this->minutes =(minutes >=0 && minutes <=59)? minutes : 0 ;
        this->seconds=(seconds >=0 && seconds <=59)? seconds : 0 ;
    }

public:
    void add_minutes(int minutes){
        this->hours += (this -> minutes + minutes) /60;
        this->minutes += (this -> minutes + minutes) %60;
    }
    void print_in_24h(){
        std::cout << hours << ':'<< minutes << ':' << seconds;
    }
    void print_in_12h(){
        if (hours > 12){
            std::cout << hours%12 << ':'<< minutes << ':' << seconds << "p. m.";
        }
        else{
            std::cout << hours%12 << ':'<< minutes << ':' << seconds << "a. m.";
        }
    }
};
class Set{
    int points_A;
    int points_B;
    int max_points;
    explicit Set(int goal):points_A(0), points_B(0),max_points(goal){};
public:
    void point_for_A(){
        points_A++;
    }
    void point_for_B(){
        points_B++;
    }
    bool end(){
        return ((points_A>= max_points|| points_B>= max_points) && (points_A>points_B+1 || points_B>points_A+1));
    }
    char winner(){
        if (!end()){
            return 'X';
        }
        return points_A>points_B ? 'A'  : 'B';
    }
    friend std::ostream &operator << (std::ostream &os, Set s);

};
std::ostream &operator << (std::ostream &os, Time t){
    t.print_in_24h();
    return os;
}
std::ostream &operator << (std::ostream &os, Set s){
    std::cout << s.points_A << ':' << s.points_B << " /" << s.max_points;
    return os;
}

std::ostream &operator<<(std::ostream &os, Point p) {
    std::cout << p.x << " " << p.y;
    return os;
}
