

#include "classes_5.h"
int indeks(char* s, char c){
    //let's assume that c is in s
    int i = -1;
    while((*(s+i+1))!=c){
        i++;
    }
    return i;
}
void drukujWspak(char *s){
    char *p = s;
    for(p; *(p); p++);
    p--;
    for(p; *(p-1); p--){
        std::cout << *p;
    }
}
void drukuj_bez_spacji(char *s){
    char *p = s;
    for(p; *(p); p++){
        if (*p == (char)32){
            continue;
        }
        std::cout << *p;
    }
}
int wystapienie(char* s, char c){
    //let's assume that c is in s
    int i = 0;
    for(char*p=s; *(p); p++){
        if (*p == c){
            i++;
        }

    }
    return i;
}
int liczba(char *s) {
    char *p = nullptr, *pointer_on_unsigned = s;
    int result = 0;
    bool Signed = false;
    if (*s == '-') {
        pointer_on_unsigned = s + 1;
        Signed = true;
    }
    for (p = pointer_on_unsigned; *(p + 1) != NULL; p++);
    result = (*p - '0');
    for (int i = 1; (p - i + 1) != pointer_on_unsigned; i++) {
        result += (*(p - i) - '0') * pow(10, i);
    }
    return Signed ? -result : result;
}

void test_liczba() {
    char *s;
    s = new char[20];
    std::cout << "Podaj liczbę by przekonwertować na int:";
    std::cin.get(s, 20);
    std::cout << liczba(s);
}

void test_5() {
    test_liczba();
}