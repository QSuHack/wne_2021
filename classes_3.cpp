
#include "classes_3.h"


long long nwd(long long a, long long b){
    if (b==0){
        return a;
    }
    return nwd(b, a%b);
}

long long nwd_it(long long a, long long b){
    while (a!=b){
        if (a > b){
            a = a-b;
        }
        else{
            b=b-a;
        }
    }
    return a;
}
int min(int a,int b ){
    return a<b ? a :b;
}
int newton(int n, int k){
    std::vector<std::vector<int>> sub;
    std::vector<int> tmp;
    tmp.assign(n+2,0);
    sub.assign(k+2 , tmp);

    int i, j;
    for (i =0; i <=n;i++){
        for (j=0; j < min(i,k);j++){
            if (j==0 || j==i){
                sub.at(i).at(j) = 1;
            }
            else{
                sub.at(i).at(j) = sub.at(i-1).at(j-1) + sub.at(i-1).at(j);
            }
        }
    }
    for(auto i: sub){
        for (auto a :i){
            std::cout << a << " ";
        }
        std::cout <<"\n";
    }
    return sub.at(n).at(k);

}



int test_3(){
    newton(10,5);
    long long nr1,nr2;
    std::cin>>nr1 >> nr2 ;
    std::cout << nwd(nr1,nr2) << " " <<  nwd_it(nr1,nr2);
    system("pause");
    return 0;
}